# Paymob Symfony Bundle

## Installation

1. Install the Paymob Payment bundle for Symfony via [paymob/symfony-bundle](https://packagist.org/packages/paymob/symfony-bundle) composer.
```bash
composer require paymob/symfony-bundle
```

2. Add the below route in the `config/routes.yaml` file of your project:

```yaml
paymob_symfony:
    resource: '@PaymobSymfonyBundle/Resources/config/routing.yaml'
```

3. In case your Application doesn't use Symfony Flex, you should enable the bundle manually by adding below line in `config/bundles.php` file:

```php
return [
    // ...
    Paymob\SymfonyBundle\PaymobSymfonyBundle::class => ['all' => true]
];
```
4. Customize the process and callback actions that exist in vendor/paymob/symfony-bundle/src/Controller/ProcessController.php file as per your needs.

## Configuration
### Paymob Account
1. Login to the Paymob account → Setting in the left menu. 
2. Get the Secret, public, API keys, HMAC and Payment Methods IDs (integration IDs).

### Merchant Configurations
1. Edit the vendor/paymob/symfony-bundle/src/Resources/config/services.yaml file and paste each key in its place.
2. Please ensure adding the integration IDs separated by comma ,. These IDs will be shown in the Paymob payment page. 
3. Copy integration callback URL that exists in services.yaml file,replace only the  {YourWebsiteURL}  with your site domain. Then, paste it into each payment integration/method in Paymob account.

> https://{YourWebsiteURL}/paymob/callback

4. Below URL is considered as your website payment process for Paymob Payment. Just replace the {YourWebsiteURL} with the actual domain.

> https://{YourWebsiteURL}/paymob/process
